export function lastElement<T>(array: Array<T>): T {
  return array[array.length - 1];
}
